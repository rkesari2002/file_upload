
# File Upload API with CI/CD

A secure file upload API using *Python* and *Flask*. Implemented a CI/CD pipeline to ensure continuous integration and deployment using *Gitlab CI* and *AWS ECS*

## Application Overview:

#### Python API Development:

- RESTful API that allows users to upload files securely.
- Implemented user authentication and authorization to control access to the file upload functionality.
- Ensure uploaded files are stored securely, preventing unauthorized access.


#### DevOps Implementation:
- Containerize the API using Docker.
- Set up a version control system using GitHub.
- Implement a CI/CD pipeline using GitLab CI.
- Automate testing
- Pushing new docker image to DockerHub
- Running AWS EWS for deployment
- Used environment variables for sensitive information, and ensure secrets are securely managed.

## Technologies/Framework/Libraries Used:

#### Flask
Flask is a micro web framework written in Python. It is classified as a microframework because it does not require particular tools or libraries. It has no database abstraction layer, form validation, or any other components where pre-existing third-party libraries provide common functions

#### SQLite
SQLite is a self-contained, serverless, zero-configuration, transactional SQL database engine. It is a lightweight and compact database that does not require any kind of server to run. It is easily integrated into any kind of mobile application

#### REST API
A REST API is a popular way for systems to expose useful functions and data. REST, which stands for representational state transfer, can be made up of one or more resources that can be accessed at a given URL and returned in various formats, like JSON, images, HTML, and more.

#### Werkzeug
Werkzeug is a collection of libraries for building Web Server Gateway Interface (WSGI) compliant web applications in Python. It started as a simple collection of utilities for WSGI applications and has grown into one of the most advanced WSGI utility libraries.

In this application it is used to perform password hashing and secure filname check

#### SQLAlchemy
SQLAlchemy is often used as an Object Relational Mapper (ORM) tool. ORM is a technique used to create a "bridge" between object-oriented programs and relational databases. SQLAlchemy translates Python classes to tables on relational databases and automatically converts function calls to SQL statements

#### Flask-JWT-Extended
Flask-JWT-Extended is a Flask extension that provides support for JSON Web Tokens (JWT) for authentication and authorization. It includes many features to make working with JWTs easier, such as automatic user loading, custom claims validation, refresh tokens, and token revoking/blocklisting.

#### Flask-Migrate
Flask-Migrate is an extension for Flask applications that handles database migrations for SQLAlchemy using Alembic. Database operations are available through the Flask command-line interface.

Command Like:
- ```flask db init```
- ```flask db migrate```
- ```flask db upgrade```

#### Gunicorn
Gunicorn is a web server for Python applications. It receives requests from a client and forwards them to Python applications or web frameworks, such as Flask or Django. Gunicorn is a WSGI-compliant server that can run any Python application concurrently.

#### Pytest
Pytest is a popular testing framework for Python that simplifies the process of writing and running test cases. It provides a clean and concise syntax for writing tests. Pytest offers powerful features for test discovery, test execution, and reporting

## API Reference

### Health Check

```https
  GET /health
```

**Description:** 

Health check endpoint to verify the health status of the service.

**Response:**
- 200 OK on success with a JSON response containing a "msg" key indicating healthiness.

**Sample Response (200 OK):**
```json
{
    "msg": "Healthy"
}
```

### Sign Up

```https
  POST /signup
```

| Parameter | Type     | Description                       |
| :-------- | :------- | :-------------------------------- |
| `username`      | `string` | **Required**. New username |
| `password`      | `string` | **Required**. New password |

**Description:** 

Registers a new user with the given username and password.

**Request Body**
```json
{
  "username": "example",
  "password": "your_password"
}
```
**Response:**
- `201 Created` on successful registration with a JSON response:
```json
{
  "msg": "Signup successful",
  "username": "example",
  "user_id": "user_id_here"
}
```
- `400 Bad Request` if username or password is missing with a JSON response:
```json
{
  "msg": "Username and password required"
}
```
- `400 Bad Request` if the username already exists with a JSON response:
```json
{
  "msg": "Username and password required"
}
```



### Log In

```https
  POST /login
```

| Parameter | Type     | Description                       |
| :-------- | :------- | :-------------------------------- |
| `username`      | `string` | **Required**. User's username |
| `password`      | `string` | **Required**. User's password |

**Description:** 

Logs in a user with the provided username and password.

**Request Body**
```json
{
  "username": "example",
  "password": "your_password"
}
```
**Response:**
- `200 OK` on successful login with a JSON response containing the user's access token, id, and username:
```json
{
  "access_token": "token_here",
  "id": "user_id_here",
  "username": "example"
}
```
- `401 Unauthorized` if login fails with a JSON response:
```json
{
  "msg": "Invalid username or password"
}
```
- `400 Bad Request` if username or password is missing with a JSON response:
```json
{
  "msg": "Username and password required"
}
```

### Upload File

```https
POST /upload
```

| Parameter | Type     | Description                       |
| :-------- | :------- | :-------------------------------- |
| `access_token`      | `string` | **Required**.The authentication token for the user making the request should be passed in header |
| `file`      | `file` | **Required**. File to upload |

**Description:**

Uploads a file to the server and associates it with the logged-in user.

**Request Body:**

Form-data with the file to be uploaded.

**Response:**
- `201 Created` on successful upload with a JSON response:
```json
{
  "msg": "File uploaded successfully",
  "filename": "uploaded_filename",
  "user_id": "user_id_here",
  "file": "download_link_here"
}
```
- `400 Bad Request` if file type is not allowed with a JSON response:
```json
{
  "msg": "File type not allowed"
}
```
- `400 Bad Request` if no file is selected with a JSON response:
```json
{
  "msg": "No selected file"
}
```

### Get Files

```https
GET /files
```
**Description:**

Returns a list of files uploaded by the authenticated user.

| Parameter | Type     | Description                       |
| :-------- | :------- | :-------------------------------- |
| `access_token`      | `string` | **Required**.The authentication token for the user making the request should be passed in header |

**Response:**
- `200 OK` on success with a JSON response containing a list of uploaded files:
```json
{
  "files": [
    {
      "filename": "file_name_here",
      "time_created": "timestamp_here",
      "download_link": "download_link_here",
      "user_id": "user_id_here"
    },
    {
      "filename": "another_file_name",
      "time_created": "timestamp_here",
      "download_link": "download_link_here",
      "user_id": "user_id_here"
    }
  ]
}
```
- `404 Not Found` if no files are found for the user with a JSON response:
```json
{
  "msg": "No files found for this user"
}
```

### Download File

```https
GET /files/<filename>
```
**Description:**

Download a file from the server.

| Parameter | Type     | Description                       |
| :-------- | :------- | :-------------------------------- |
| `access_token`      | `string` | **Required**.The authentication token for the user making the request should be passed in header |

**Response:**
- The file to be downloaded with `200 OK` status code if the file exists.
- `404 Not Found` if the user or file is not found with a JSON response:
```json
{
  "msg": "File not found"
}
```

#### Postman Collection

```txt
File_upload.postman_collection.json
```
Postman Collection File is provided in the Repository. Download and Import it in the postman to start making request to the APIs 
## Run Locally

To downlaod this project

```bash
  git clone https://gitlab.com/rkesari2002/file_upload.git
```

Go to the project directory

```bash
  cd my-project
```

Create a Virtualenv
```bash
  Linux/MacOS: python3 -m venv env
  Windows: py -m venv env
```

Start Virtualenv
```bash
  Linux/macOS: source env/bin/activate
  Windows: .\env\Scripts\activate
```

Install all the dependencies 

```bash
  pip install -r requirements.txt
```


Configure the Environment Varaible

```bash
  Change name of ".env-copy" to ".env"
```
```txt
  Configure all the Variable in .env as per your requirements
  To creake a random SECRET_KEY run:
        - openssl rand -hex 32
    Set DEBUG =  True

```

Initialize the db

```python
  flask db init
```

Make migrations for db

```python
  flask db migrate
```
Note:
```txt
Ignore if this Error: Meaning some migrations exsists

Error: Directory migrations already exists and is not empty
```

Migrate to db

```python
  flask db upgrade
```

Runserver Locally

```python
  python app.py
```

#### Your server shoud be running Locally!!!

## Running Tests

Command:
```bash
pytest test.py
```


## Run Inside Docker



#### Install Docker Engine

```https
https://docs.docker.com/engine/install/
```

#### Create Docker Image

```bash
docker build -t <your-image-name> .
```
Note:  cd in the directory where Dockerfile exsists

#### Run Docker Image

```bash
docker run -p 5000:5000 <your-image-name>
```
## Deployment

To deploy this Push it to Gitlab Repository

- Initialize git
```bash
  git init
```
- Git add
```bash
  git add .
```
- Git commit
```bash
  git commit -m "<message>"
```
- Git add origin
```bash
  git remote add origin <origin_link>
```
- Git branch main
```bash
  git branch -M main
```
- Git Push to main
```bash
  git push -uf origin main
```
Note: If the code is not pushed to main branch directly create another branch and then raise a merge request to merge in main branch or change the main to unprotected

- Create a account in Dokcerhub
```https
https://hub.docker.com/signup
```
- Create DockerHub Access Token
```txt
Account Settings > Security > Access Token
```
- Create a new Dokcerhub Repository
- Change DOCKERHUB_REPO to this repo's link in .gitlab-ci.yml file

- Create a account in AWS
```https
https://aws.amazon.com/resources/create-account/
```
- Create a new ECS cluster
- Create a new Task in ECS SERVICE
- Create a service in the cluster with the Task
- Configure the load Balancer as per requirements and set the health path as /health
- If the service is running and health route is working but the request is not able to go thorugh from outside then configure the Security group of Load Balancer's VPC to receive request from all ip(s)

#### Create Variables in Gitlab Repository's Settings > CI/CD > Variables

| Key | Value     | 
| :-------- | :------- | 
| `AWS_ACCESS_KEY_ID`      | `Get AWS ACCESS KEY ID from AWS IAM` |
| `AWS_SECRET_ACCESS_KEY`      | `Get AWS ACCESS SECRET KEY from AWS IAM` | 
| `AWS_DEFAULT_REGION`      | `Region where you want your ECS cluster is in` | 
| `AWS_ECS_CLUSTER`      | `AWS CLUSTER NAME` | 
| `AWS_ECS_SERVICE`      | `AWS SERVICE NAME inside CLUSTER` | 
| `CI_REGISTRY_USER`      | `DockerHub username` | 
| `CI_REGISTRY_PASSWORD`      | `DockerHub access_key` |
| `JWT_SECRET_KEY`      | `COPY JWT_SECRET_KEY from local` |
| `DEBUG`      | `False` |


Note: If the main branch is set as unprotected make sure the these Variables are also not protected




## CI/CD PipeLine Explaination
#### Gitlab
Git Lab is used for version control and Countinous Integration of Code. Multiple user can work on different branches and upon completeion the code can be merged to main branch

#### Gitlab CI PipeLine Stages
- Test
- Build
- Deploy


#### Test
Code for running the test stage is written here
- Run a image of python service
- Install all requirements
- Run test command
- Upon success move to next stage

#### Build
In this stage a dockerimage image is build and pushed to DockerHub
- Run a image of docker service
- Creation of docker image
- Login into docker registry
- Push the latest docker image to dockerhub
- Upon success move to next stage

#### Deploy
In the stage we Deploy our latest docker image to ECS
- Run a image of docker service
- Install awscli to interact with aws
- Configure AWS
- Execution of AWS ECS update command


### FLow of CI/CD and Deployment
![CI/CD Image](https://gitlab.com/rkesari2002/file_upload/-/raw/main/readme_img/ci_cd_img.png?ref_type=heads)
-  Deveoplment will be Done in Local Enviorment
- Raise a Merge request to main branch upon completeion of task or feature
- Review the code and accept the Request
- CI/CD PipeLine will be executed automatically
- Upon successfull of PipeLine code will be pushed to AWS ECS and will be rolled out
- Load Balancer is set up to traffic
- You can access the Load Balancer's Public address to make request to the service
