"""
Health check route
"""
from flask import Blueprint, jsonify, request
from flask_jwt_extended import create_access_token
from models import User
from models import db

health_bp = Blueprint('health', __name__)

@health_bp.route('/health', methods=['GET'])
def health():
    """
    Health check route
    """
    return jsonify({"msg": "Healthy"}), 200
